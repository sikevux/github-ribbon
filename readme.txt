=== Github Ribbon ===
Contributors: sudar, sikevux
Tags: github, ribbon, git
Requires at least: 2.8
Donate Link: http://sudarmuthu.com/if-you-wanna-thank-me
Tested up to: 3.0.4
Stable tag: 0.4

Adds "Fork me on Github" ribbons to your WordPress posts

== Description ==

Adds "Fork me on Github" ribbons to your WordPress posts

### Translation

* Dutch (Thanks Rene of [WPwebshop ][2])

The pot file is available with the Plugin. If you are willing to do translation for the Plugin, use the pot file to create the .po files for your language and let me know. I will add it to the Plugin after giving credit to you.

### Support

Support for the Plugin is available from the [Plugin's home page][1]. If you have any questions or suggestions, do leave a comment there.

 [1]: http://sudarmuthu.com/wordpress/github-ribbon
 [2]: http://wpwebshop.com/
	
== Installation ==

Extract the zip file and just drop the contents in the wp-content/plugins/ directory of your WordPress installation and then activate the Plugin from Plugins page.

== Screenshots ==

1. Plugin in action in author's blog

2. Configuration screen

== Changelog ==

###v0.1 (2010-08-04)

*   Initial Release

###v0.2 (2010-11-08)

*   Added option to use CSS3 ribbons

###v0.3 (2011-01-23)

*   Added Dutch translations

###v0.4 (2011-04-25)

*	User enters username instead of full URL to GitHub Profile

==Readme Generator== 

This Readme file was generated using <a href = 'http://sudarmuthu.com/wordpress/wp-readme'>wp-readme</a>, which generates readme files for WordPress Plugins.
